#include <limits.h>
#include "gtest/gtest.h"
#include "Multiply.hpp"
#include "Addition.hpp"

class MathTest : public ::testing::Test{
    protected:

	Multiply multi;
	Addition add;

	virtual void SetUp(){
	    std::cout << "SetUp start Math Test" << std::endl;
	}

	virtual void TearDown() {
	    std::cout << "TearDown end Math Test" << std::endl;
	}

};

TEST_F(MathTest, twoValues){

    const int x = 4;
    const int y = 5;

    EXPECT_EQ(20, multi.twoValues(x, y));
    EXPECT_EQ(9,  add.twoValues(x, y));

}

TEST_F(MathTest, twoValues1){

    EXPECT_EQ(1, multi.twoValues(1, 1));
    EXPECT_EQ(2, add.twoValues(1, 1));

}

